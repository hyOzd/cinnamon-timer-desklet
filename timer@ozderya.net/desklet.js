
const Lang = imports.lang;
const Mainloop = imports.mainloop;
const St = imports.gi.St;
const Clutter = imports.gi.Clutter;
const CinnamonDesktop = imports.gi.CinnamonDesktop;
const GLib = imports.gi.GLib;
const Main = imports.ui.main;
const Settings = imports.ui.settings;
const Desklet = imports.ui.desklet;

const TIMEOUT = 5000; // millisconds
const UPDATE_TIMEOUT = 20; // milliseconds

const SIZE = 200;
const RING_WIDTH = 10;

const STATE = Object.freeze(
	{
		IDLE : "IDLE",        // start state
		RUNNING : "RUNNING",  // timer is running (by clicking "Start")
		PAUSED : "PAUSED",    // timer is paused (by clicking "Pause")
		FINISHED : "FINISHED" // timer has finished (user hasn't clicked "Done")
	});

function MyDesklet(metadata, desklet_id){
    this._init(metadata, desklet_id);
}

MyDesklet.prototype =
{
    __proto__: Desklet.Desklet.prototype,

    _init: function(metadata, desklet_id)
    {
        Desklet.Desklet.prototype._init.call(this, metadata);
		this._state = STATE.IDLE;

		// setup UI
		this._ui = new St.Group({style_class : "timer-desklet"});
		this.setContent(this._ui);

		this._background = new St.DrawingArea({width:SIZE, height:SIZE});
		this._ui.add_actor(this._background);
		this._background.connect("repaint", Lang.bind(this, this._paintBackground));

        this._vBox = new St.BoxLayout({vertical : true});
		this._vBoxBin = new St.Bin({width: SIZE, height: SIZE,
									y_align: St.Align.MIDDLE});
		this._vBoxBin.set_child(this._vBox);
		this._ui.add_actor(this._vBoxBin);

		this._title = new St.Label({text: "Timer"});
        this._vBox.add(this._title, {x_fill :false, x_align: St.Align.MIDDLE});

		this._timeLabel = new St.Label({text : "00:00:000", style_class:"time-label"});
        this._vBox.add(this._timeLabel, {x_fill: false, x_align: St.Align.MIDDLE});

        this._startButton = new St.Button({label : _("Start")});
        this._vBox.add_actor(this._startButton);
        this._startButton.connect("clicked", Lang.bind(this, this._startStop));

        this.setHeader(_("Timer"));

		// initialize settings
		this.settings = new Settings.DeskletSettings(this, metadata["uuid"], desklet_id);
		this.settings.bindProperty(this, "minutes", "setting_minutes",
								   this._onSettingsChanged, null);
		this.settings.bindProperty(this, "seconds", "setting_seconds",
								   this._onSettingsChanged, null);
		this.settings.bindProperty(this, "title", "setting_title",
								   this._onTitleSettingChanged, null);
		this.settings.bindProperty(this, "message", "setting_message",
								   this._onTitleSettingChanged, null);
		this.settings.bindProperty(this, "foreground_color", "setting_foreground_color",
								   this._onColorSettingsChanged, null);
        this.settings.bindProperty(this, "background_color", "setting_background_color",
								   this._onColorSettingsChanged, null);

        //
        this._timerSetting = 0;     // timer is set to this value
		this._timeoutLength = 0;    // length of the currently running timeout
		this._onSettingsChanged();
		this._onTitleSettingChanged();
		this._onColorSettingsChanged();
    },

	_setTimer: function(timerLength)
	{
		if (this._state == STATE.RUNNING)
		{
			GLib.source_remove(this._timer);
			GLib.source_remove(this._updateTimer);
		}
		this._timerSetting = timerLength;
		this._timeoutLength = timerLength;
		this._timeLabel.set_text(this._formatTime(timerLength));
		this._startButton.set_label("Start");
		this._state = STATE.IDLE;
		this._background.queue_repaint();
	},

	_onSettingsChanged : function()
	{
		let ms = this.setting_minutes * 60 * 1000 + this.setting_seconds * 1000
		this._setTimer(ms);
	},

	_onTitleSettingChanged : function()
	{
		this._title.set_text(this.setting_title);
	},

	_onColorSettingsChanged: function()
	{
		s = "color:" + this.setting_foreground_color + ";"
		s += "circle-bg-color:" + this.setting_background_color + ";"
		this._ui.set_style(s);
	},

    _startStop: function()
    {
        if (this._state == STATE.IDLE || this._state == STATE.PAUSED)
        {   // start/continue timer
            this._startTime = GLib.get_monotonic_time() / 1000;

            this._timer = Mainloop.timeout_add(
                this._timeoutLength, Lang.bind(this, this._timeOut));

            this._updateTimer = Mainloop.timeout_add(
                UPDATE_TIMEOUT, Lang.bind(this, this._update));

			this._startButton.set_label("Pause");

			this._state = STATE.RUNNING;
        }
		else if (this._state == STATE.RUNNING)
		{   // pause timer
			GLib.source_remove(this._timer);
			GLib.source_remove(this._updateTimer);
			let pastTime = GLib.get_monotonic_time() / 1000 - this._startTime;
			this._timeoutLength -= pastTime;

			this._startButton.set_label("Continue");

			this._state = STATE.PAUSED;
		}
        else if (this._state == STATE.FINISHED)
        {   // reload timer to previous value
			this._setTimer(this._timerSetting);
        }
		this._background.queue_repaint();
    },

    _update: function()
    {
        if (this._state == STATE.RUNNING)
        {
			let pastTime = GLib.get_monotonic_time() / 1000 - this._startTime;
			let leftTime = this._timeoutLength - pastTime;
			leftTime = Math.round(leftTime);
			if (leftTime < 0) leftTime = 0;

			this._timeLabel.set_text(this._formatTime(leftTime));
			this._background.queue_repaint();

            return true;
        }
        else
        {
            return false;
        }
    },

    _timeOut: function()
    {
        this._state = STATE.FINISHED;
		this._startButton.set_label("Done");
		this._timeLabel.set_text(this._formatTime(0));
		Main.notify(this.setting_message);
		global.play_theme_sound(0, "alarm-clock-elapsed");

		this._background.queue_repaint();

        return false;
    },

    _formatTime: function(milliseconds)
    {
        let seconds = Math.floor(milliseconds / 1000);
        milliseconds -= seconds*1000;
        let minutes = Math.floor(seconds / 60);
        seconds -= minutes*60;

        let strMinutes = minutes.toString();
        if (strMinutes.length == 1) strMinutes = "0" + strMinutes;
        let strSeconds = seconds.toString();
        if (strSeconds.length == 1) strSeconds = "0" + strSeconds;

        let strMilliseconds = milliseconds.toString();
        if (strMilliseconds.length == 1)
        {
            strMilliseconds = "00" + strMilliseconds;
        }
        else if (strMilliseconds.length == 2)
        {
            strMilliseconds = "0" + strMilliseconds;
        }

        return strMinutes + ":" + strSeconds + ":" + strMilliseconds;
    },

	_paintBackground: function()
	{
		// get the completion ratio to draw the ring
		if (this._state == STATE.RUNNING)
		{
			let pastTime = GLib.get_monotonic_time() / 1000 - this._startTime;
			let leftTime = this._timeoutLength - pastTime;
			leftTime = Math.round(leftTime);
			if (leftTime < 0) leftTime = 0;
			var completionRatio = 1 - leftTime / this._timerSetting;
		}
		else if (this._state == STATE.PAUSED)
		{
			var completionRatio = 1 - this._timeoutLength / this._timerSetting;
		}
		else
		{
			var completionRatio = 1;
		}

		let cr = this._background.get_context();

		let [hasColor, bgColor] =
			this._background.get_theme_node().lookup_color("circle-bg-color", true);
		if (!hasColor) bgColor = new Clutter.Color({alpha:255});

		let ringColor = this._background.get_theme_node().get_foreground_color();
		cr.save();

		// draw the background circle
		cr.setSourceRGBA(bgColor.red/255,
						 bgColor.green/255,
						 bgColor.blue/255,
						 bgColor.alpha/255);
		cr.arc(SIZE/2, SIZE/2, SIZE/2 - RING_WIDTH, 0, 2*Math.PI);
		cr.fill();

		// draw the ring
		if (this._state != STATE.IDLE)
		{
			if (completionRatio == 1) // draw complete ring
			{
				cr.arc(SIZE/2, SIZE/2, SIZE/2-RING_WIDTH/2, 0, 2*Math.PI);
			}
			else // draw progress ring
			{
				cr.arc(SIZE/2, SIZE/2, SIZE/2-RING_WIDTH/2,
					   -Math.PI/2, 2*Math.PI*completionRatio-Math.PI/2);
			}
			cr.setSourceRGB(ringColor.red/255, ringColor.green/255, ringColor.blue/255);
			cr.setLineWidth(RING_WIDTH);
			cr.stroke();
		}

		cr.restore();
		cr = null;
	}
}

function main(metadata, desklet_id){
    let desklet = new MyDesklet(metadata, desklet_id);
    return desklet;
}
